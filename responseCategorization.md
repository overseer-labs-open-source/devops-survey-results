In order to turn people's free-form responses to a few survey questions into
quantifiable data, I had to try to capture some common themes and then categorize
each response. This was a little challenging, as sometimes responses seemed
like they could fit in multiple categories (does this tomato response belong in
the fruit bucket or the vegetable bucket? if only there was established taxonomic
criteria for surveys!). Also, sometimes people listed multiple things- in this case
I usually just went with the first part of their response to guide where I put it.

There also might be some punctuation differences here between what people put in
their answers and how they appear on this page (due to the fact that I'm pulling these
from the csv where commas have been replaced with semicolons or periods).

Without further ado, here are the categorizations:

## Wishes

### More understanding of DevOps value

- Our definition was a bit more defined. Currently acting as Release Engineer / Automation Engineer / Firewall Rules Guy / SysAdmin
- Others in my organization and on my team would embrace the concepts.
- I had more people that understand what im trying to explain to them; our CTO might be good at application architecture or structure but has no clue what is involved in the infrastructure side of things.
Whish I had more time to focus on our existing infrastructure instead of constantly working on new projects.
- I wish the other engineers I work with understood DevOps at all
- That our developers and the overall org could understand that we are trying to make things better not worse.
- The teams were involved and embracing it more. Sometimes I feel like I'm grabbing a bunch of kids by their hands and showing the playground they don't want to play on.

### More time

- We had more time to automate than deal with ops. Coming from a company of 200k+ employees
- More developers cared about their own quality of life.
- I had more hours in the day... and I wish I was better at writing code.
- to work more with infrastructure code; less with applications **Note: This one was
a little hard, I took it to mean that the person doesn't have time to automate stuff because
it's being spent working on applications. I could also see it as "other technical"**

### Management communication

- Management understood time for automation and engineering pays off 10 fold to cut-down repetitive tasks that eat time; mature the Ops service; improve Developer time; and is the only path to more 9's. So give us dedicated time.
- Business would stick to a plan
- That more business-sided people understood that deployment and operations is just as important a software component as the rest of the application.
- that management would live up to their own internal policies of openness and experimentation instead of shutting down anything they don't understand. Also I wish they would have more respect: respect for employee time; respect for flexibility; respect for the sanctity and immutability of the Sprint; and respect the employees themselves by trusting them and acknowledging when they know best.
- My manager would better manage and level my work stream. Planning for things would happen in advance. That there was space to learn entirely new platforms and tools without constant pressure from production issues.

### Growth opportunities

- I had better growth opportunities. Dev doesn't seem to need my skills and have rejected proposals to improve CI; CD; deploys; security as not priorities. Limited hierarchy means no upward tragectory either
- I had Mentoring
- I could do more high level stuff

### Help/guidance

- I could teach more people to do my job.
- Had more people and I wasn't so deep
- More guidance
- I wasn't the only one in the company who has an idea how our application stack works and talks to its components.
- I had more help

### More ops/dev integration

- To move to Dev position
- That more Developers would embrace automation. Also that the Business side of the house understands that automating a large system takes time.
- being thought of as an application developer was not a thing
- More managers and tech workers got on board with DevOps being a process and methodology and not strictly a position a person has.
- DevOps were better defined and adopted as a culture and not a job title.
- To build better teams with stronger practices that empower more people to take responsibility for their work all the way through production.
- The management actually transitioned some of my responsibilities over the development team and let me automate the pipeline process instead of constantly bombarding us with trivial tasks that takes hours to do manually; but there's zero incentive to automate because they won't wait for automation; nor give us the budget to do so
- I had more opportunity to interact with dev and code; design architecture
- I had more insight in our application code; Cloudformation was natively ruby
- I was less siloed. I have to actively fight to be included in feature planning and road mapping; if I don't invite myself I'm not invited.
- As an DevOps person; I wish more companies would get on board about the DevOps culture change instead of creating another silo
- To have first class support from my developers and have them help all of us win the fight. Things we should all care about don't make business impact.
- Monitoring and configuration management tools valued modularity and flexibility more; and were held to the same standards tools for any other use are. - There were more interoperability standards such that different tools with different strengths could be integrated without a huge amount of glue code - People actually understood that DevOps is a mindset and process; not one person to whom you can dump every deployment/monitoring/whatever task needed - Developers understood basic infrastructure needs and principles; and actually took them into account when developing software that will need to be managed in production
- I wish developers did a better job of making their processes repeatable.

### Better documentation

- for better documentation and less reliance on our longtime contractor's knowledge
- 3rd party packages had better documentation. And I wish people would google their error messages before interrupting my work.

### Less tech debt

- Software wasn't so fragile. These days it feel like if you breathe on something wrong the entire stack can collapse. I would think I was just paranoid; but then you had stuff like the left-pad issue with Node.
- I could get my organization to adopt a consistent deployment process (across dev; staging and prod); even if it's at the cost of the most efficient one for any one environment.
- The infrastructure I inherited wouldn't catch on fire so I can build it the right way
- More of the things I maintain were set up correctly the first time

### Other cultural

- we didnt rely so much on the knowledge of a longtime contractor who's rolling off our account
- We could operate in a more lean fashion.
- we had more code focused workers
- We could get past arguing over which tool is best and move onto what fundamental concepts fit which use cases
- the industry would be less hype driven and creators of those hypetrains would work together more to work on standardized soltutions rather than trying to compete with a product that does nearly the same than competitors.

### Other technical

- My organization was open to using more serverless technologies. That I worked with larger applications/harder problems/greater scale. I had more opportunities to try out management.
- that having 13 monitors wasn't considered "unreasonable"
- Tool developers use principle " easy to do; possible to customize"
- That optimization resources and scalability could be explain more easily
- docker had a proper configuration management pluggable interface/api instead of dockerfiles; people were less cynical about new tools; more ops people were more open about learning development; test automation were more pervasive; service management and project management weren't seen as completely different
- to have all of my infrasstructure as a code monitorized;In other hand I wish to have all deployments across pipelines with high test coverture.
- **Note: I somehow missed this one before I wrote the post, so it's not in the
numbers there. But I would have categorized it as other technical** I could make
 all sites identical and unify implementation standard.

## Favorite parts

### Business value added

- The control and signifucance team role in enter orgsnization in achieving 99.999 % uptime
- The wide level of visibility and impact of what I am working on.
- Always new tools/tech to help improve the business.
- My favorite part of being an DevOps person is building meaningful internal tools to make other people's job easier
- the job is evolving a lot; lots of opportunities to make things better/work differently
- When we finally make something better and the skeptics are surprised how much better life is after we've made the change.
- Building tooling that is useful to the company; saves us money; improves the customer experience or makes the development process more streamlined.

### Cool tech

- Cutting edge tools
- Presently the concept of infrastructure as code
- Bleeding edge of the bleeding edge

### Fun challenge

- solving problems
- solving code puzzles
- Being challenged with the problems on both sides of the fence (dev vs prod)
- Solving hard problems and making a difference
- Watching things break then putting them back together again.
- Fixing problems and issues; its super rewarding to me personally.
- All the possibilities of finding solutions to problems; the variety of problems to solve; the current landscape of tooling. Automating things is fun. Automating things and showing quality or development "now you can do this thing by clicking this button" and seeing their delight is satisfying.
- The Challenges that require thought and planning but being able to implement and make them work. Is rewarding.
- My team and architecting solutions to complex problems.

### Joy of automation

- Watching automation succeed
- Automation mindset; flexible work schedule; and constantly trying out new things.
- automating
- Automating things and graphs
- I get to automate all the boring stuff that comes with being an Ops/Sysadmin; but I still get to have full control of the infrastructure that runs the applications myself and the other developers write.
- Automating everything
- Automate process
- Automating
- DevOps all the things! Seriously though I like automation
- Automation
- Discovering ways to work less and product more. Automate things monitorize them and go home in weekend in pace.

### Satisfaction of a job well done

- Being responsible.
- the stuff works and works well.
- When things work; really well

### Other technical

- Designing the code pipeline; building; designing
- Designing systems
- Getting to plan and work on complex systems with a higher concentration of "fun parts" than development (where I'd spend less time worrying about core system functionality and more about UI; tests; API interoperability; etc)
- architecture;system design
- creating tools to manage infrastructure

### System mastery

- Being able to watch the whole process from checking to running on servers in less than a few minutes; all automaticallty.
- Making robots and making them smarter to handle giant scale that individuals could never control.
- I know the system inside and out from top to bottom. If something's in anybody's way I have the ability to take some sort of action. I enjoy being further away from user-facing product development and working with systems and not trying to figure out the market.
- seeing the big picture. Touching every part of the stack. Driving forward the ideology of DevOps.
- Working with distributed systems - Working with operating systems - Having a complete view of a multi-software system and understanding how it works - Being able to use knowledge in the whole spectrum of abstraction levels from high-level application code to low-level performance/debugging
- getting to play with the whole stack up and down

### Variety

- No two days are the same and being able to come up with creative solutions to problems.
- Learning something new almost every day. Working at a small company I get the chance to work with different parts of our Engineering department.
- learning new s#!+
- Touching every part of the business.
- Being able to design and build (code) new things all the time; usually the way I want it so I don't get woke up in the middle of the night.
- Always learning and absorbing new technologies
- Broad involvement across the software lifecycle and ability to interact with many slices of technology and teams.
- Every day is different
- Variety. Getting to learn new technologies. Potential for lots £$€ is very high.
- The diversity of the work is exhausting but exciting. One day I find myself doing code reviews of a failed build with a developer to try and understand why CPU increases caused a massive scaling issue. The next day I might find myself troubleshooting tedious permissions issues with an AWS Data Pipeline.
- Researching; designing and implementing new stuff.

### Other cultural

- Nothing. It is just something that tends to happen in the industry.
- Iterative ease
- Communication with smart people
- Teaching people about distributed systems
- Knowing that there's always something new and exciting around the corner
- Not though guidance
- Fun
