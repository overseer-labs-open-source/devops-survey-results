import argparse
import csv
import os
import math

def analysis_list():
    return [title_analysis(), team_size_analysis(), team_number_analysis(), 
            time_analysis(), concern_analysis(), wish_analysis(), favorites_analysis(), 
            architecture_analysis(), user_access_analysis(), infra_analysis(),
            server_count_analysis(), metric_count_analysis()]

def main():
    parsed_args = parse_args()
    file_path = parsed_args.file_path
    if (not os.path.exists(file_path)) or os.path.isdir(file_path):
        raise ValueError("'{}' is not a file".format(file_path))    
    
    with open(file_path, 'r') as f:
        process_file(f)

class team_number_analysis(object):
    def __init__(self):
        self.all_team_number_analysis = category_analysis("Dedicated teams", [
                ("none",create_field_filter("Teams dedicated to DevOps/SRE", "0"), "0"),
                ("one",create_field_filter("Teams dedicated to DevOps/SRE", "1"), "1"),
                ("few",create_field_filter("Teams dedicated to DevOps/SRE", "2-5"), "2-5"),
                ("many",create_field_filter("Teams dedicated to DevOps/SRE", "6-9"), "6-9"),
                ("lots",create_field_filter("Teams dedicated to DevOps/SRE", "10+"), "10+"),
                ("unknown",create_field_filter("Teams dedicated to DevOps/SRE", "unknown"), "I don't know"),
            ],
            create_question_answered_filter("Teams dedicated to DevOps/SRE"))
        
        self.soloists_team_number_analysis = category_analysis("Dedicated teams (solo teams)", [
                ("none",and_(create_field_filter("Teams dedicated to DevOps/SRE", "0"), create_field_filter("People on team", "1")), "0"),
                ("one", and_(create_field_filter("Teams dedicated to DevOps/SRE", "1"), create_field_filter("People on team", "1")), "1"),
                ("few", and_(create_field_filter("Teams dedicated to DevOps/SRE", "2-5"), create_field_filter("People on team", "1")), "2-5"),
                ("many",and_(create_field_filter("Teams dedicated to DevOps/SRE", "6-9"), create_field_filter("People on team", "1")), "6-9"),
                ("lots",and_(create_field_filter("Teams dedicated to DevOps/SRE", "10+"), create_field_filter("People on team", "1")), "10+"),
                ("unknown",and_(create_field_filter("Teams dedicated to DevOps/SRE", "unknown"), create_field_filter("People on team", "1")), "I don't know"),
            ],
            and_(create_question_answered_filter("Teams dedicated to DevOps/SRE"), create_field_filter("People on team", "1")))
        
        self.med_team_number_analysis = category_analysis("Dedicated teams (mid-size teams)", [
                ("none",and_(create_field_filter("Teams dedicated to DevOps/SRE", "0"), create_field_filter("People on team", "2-9")), "0"),
                ("one", and_(create_field_filter("Teams dedicated to DevOps/SRE", "1"), create_field_filter("People on team", "2-9")), "1"),
                ("few", and_(create_field_filter("Teams dedicated to DevOps/SRE", "2-5"), create_field_filter("People on team", "2-9")), "2-5"),
                ("many",and_(create_field_filter("Teams dedicated to DevOps/SRE", "6-9"), create_field_filter("People on team", "2-9")), "6-9"),
                ("lots",and_(create_field_filter("Teams dedicated to DevOps/SRE", "10+"), create_field_filter("People on team", "2-9")), "10+"),
                ("unknown",and_(create_field_filter("Teams dedicated to DevOps/SRE", "unknown"), create_field_filter("People on team", "2-9")), "I don't know"),
            ],
            and_(create_question_answered_filter("Teams dedicated to DevOps/SRE"), create_field_filter("People on team", "2-9")))
        
        self.large_team_number_analysis = category_analysis("Dedicated teams (large teams)", [
                ("none",and_(create_field_filter("Teams dedicated to DevOps/SRE", "0"), create_field_filter("People on team", "10+")), "0"),
                ("one", and_(create_field_filter("Teams dedicated to DevOps/SRE", "1"), create_field_filter("People on team", "10+")), "1"),
                ("few", and_(create_field_filter("Teams dedicated to DevOps/SRE", "2-5"), create_field_filter("People on team", "10+")), "2-5"),
                ("many",and_(create_field_filter("Teams dedicated to DevOps/SRE", "6-9"), create_field_filter("People on team", "10+")), "6-9"),
                ("lots",and_(create_field_filter("Teams dedicated to DevOps/SRE", "10+"), create_field_filter("People on team", "10+")), "10+"),
                ("unknown",and_(create_field_filter("Teams dedicated to DevOps/SRE", "unknown"), create_field_filter("People on team", "10+")), "I don't know"),
            ],
            and_(create_question_answered_filter("Teams dedicated to DevOps/SRE"), create_field_filter("People on team", "10+")))
    
    def process_row(self, row):
        self.all_team_number_analysis.process_row(row)
        self.soloists_team_number_analysis.process_row(row)
        self.med_team_number_analysis.process_row(row)
        self.large_team_number_analysis.process_row(row)
    
    def print_results(self):
        self.all_team_number_analysis.print_results()
        self.soloists_team_number_analysis.print_results()
        self.med_team_number_analysis.print_results()
        self.large_team_number_analysis.print_results()

class team_size_analysis(object):
    def __init__(self):
        self.all_size_analysis = category_analysis("People on team", [
                ("alone",create_field_filter("People on team", "1"), "1"),
                ("med",create_field_filter("People on team", "2-9"), "2-9"),
                ("big",create_field_filter("People on team", "10+"), "10+")
            ],
            create_question_answered_filter("People on team"))
        
        self.sre_size_analysis = category_analysis("People on team (SREs)", [
                ("alone",and_(create_field_filter("People on team", "1"), create_field_filter("Job Title", "Site Reliability Engineer")), "1"),
                ("med",and_(create_field_filter("People on team", "2-9"), create_field_filter("Job Title", "Site Reliability Engineer")), "2-9"),
                ("big",and_(create_field_filter("People on team", "10+"), create_field_filter("Job Title", "Site Reliability Engineer")), "10+")
            ],
            and_(create_question_answered_filter("People on team"), create_field_filter("Job Title", "Site Reliability Engineer")))
        
        self.devops_size_analysis = category_analysis("People on team (DevOps)", [
                ("alone",and_(create_field_filter("People on team", "1"), create_field_filter("Job Title", "DevOps")), "1"),
                ("med",and_(create_field_filter("People on team", "2-9"), create_field_filter("Job Title", "DevOps")), "2-9"),
                ("big",and_(create_field_filter("People on team", "10+"), create_field_filter("Job Title", "DevOps")), "10+")
            ],
            and_(create_question_answered_filter("People on team"), create_field_filter("Job Title", "DevOps")))
        
        self.software_size_analysis = category_analysis("People on team (Software Engineer)", [
                ("alone",and_(create_field_filter("People on team", "1"), create_field_filter("Job Title", "Software Engineer")), "1"),
                ("med",and_(create_field_filter("People on team", "2-9"), create_field_filter("Job Title", "Software Engineer")), "2-9"),
                ("big",and_(create_field_filter("People on team", "10+"), create_field_filter("Job Title", "Software Engineer")), "10+")
            ],
            and_(create_question_answered_filter("People on team"), create_field_filter("Job Title", "Software Engineer")))
    
    def process_row(self, row):
        self.all_size_analysis.process_row(row)
        self.sre_size_analysis.process_row(row)
        self.devops_size_analysis.process_row(row)
        self.software_size_analysis.process_row(row)
    
    def print_results(self):
        self.all_size_analysis.print_results()        
        self.sre_size_analysis.print_results()
        self.devops_size_analysis.print_results()
        self.software_size_analysis.print_results()

class title_analysis(object):
    def __init__(self):
        other_title_filter = not_(or_(create_field_filter("Job Title", "DevOps"), 
                                      create_field_filter("Job Title", "Site Reliability Engineer"), 
                                      create_field_filter("Job Title", "Software Engineer")))
        self.analysis = category_analysis("Job title", [
                                                    ("dev_ops",create_field_filter("Job Title", "DevOps"), "DevOps"),
                                                    ("sre",create_field_filter("Job Title", "Site Reliability Engineer"), "Site Reliability Engineer"),
                                                    ("software",create_field_filter("Job Title", "Software Engineer"), "Software Engineer"),
                                                    ("other",other_title_filter, "Other"),
                                                ],
                                    lambda r : True
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()

class time_analysis(object):
    def __init__(self):
        dev_ops_filter = create_field_filter("Job Title", "DevOps")
        sre_filter = create_field_filter("Job Title", "Site Reliability Engineer")
        software_filter = create_field_filter("Job Title", "Software Engineer")
        
        self.analyses = [
            rank_analysis("Time diagnosing prod issues", "Time Diagnosing"),
            rank_analysis("Time monitoring system", "Time monitoring"),
            rank_analysis("Time automating CD", "Time Automating CD"),
            rank_analysis("Time working on features", "Feature Work"),
            rank_analysis("Time working on other things", "Time Other"),
            rank_analysis("\nTime diagnosing prod issues (DevOps)", "Time Diagnosing", dev_ops_filter),
            rank_analysis("Time monitoring system (DevOps)", "Time monitoring", dev_ops_filter),
            rank_analysis("Time automating CD (DevOps)", "Time Automating CD", dev_ops_filter),
            rank_analysis("Time working on features (DevOps)", "Feature Work", dev_ops_filter),
            rank_analysis("Time working on other things (DevOps)", "Time Other", dev_ops_filter),
            rank_analysis("\nTime diagnosing prod issues (SRE)", "Time Diagnosing", sre_filter),
            rank_analysis("Time monitoring system (SRE)", "Time monitoring", sre_filter),
            rank_analysis("Time automating CD (SRE)", "Time Automating CD", sre_filter),
            rank_analysis("Time working on features (SRE)", "Feature Work", sre_filter),
            rank_analysis("Time working on other things (SRE)", "Time Other", sre_filter),
            rank_analysis("\nTime diagnosing prod issues (Software Engineer)", "Time Diagnosing", software_filter),
            rank_analysis("Time monitoring system (Software Engineer)", "Time monitoring", software_filter),
            rank_analysis("Time automating CD (Software Engineer)", "Time Automating CD", software_filter),
            rank_analysis("Time working on features (Software Engineer)", "Feature Work", software_filter),
            rank_analysis("Time working on other things (Software Engineer)", "Time Other", software_filter),]
    
    def process_row(self, row):
        for analysis in self.analyses:
            analysis.process_row(row)
    
    def print_results(self):
        for analysis in self.analyses:
            analysis.print_results()

class concern_analysis(object):
    def __init__(self):       
        self.analyses = [
            rank_analysis("Concerns about CD", "CD Concern"),
            rank_analysis("Concerns about unknown bugs", "Unknowns Concern"),
            rank_analysis("Concerns about MTTR", "MTTR Concern"),
            rank_analysis("Concerns about team scaling", "Team Scaling Concern"),
            rank_analysis("Concerns about false positive rate", "False Positives Concern"),
            rank_analysis("Concerns about respect", "Respect Concern"),
            rank_analysis("Concerns about lack of job definition", "Job Definition Concern"),
            rank_analysis("Other concern", "Other Concern")]
    
    def process_row(self, row):
        for analysis in self.analyses:
            analysis.process_row(row)
    
    def print_results(self):
        for analysis in self.analyses:
            analysis.print_results()

class wish_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("Wishes", [
                                                    ("dev_ops_integration",create_field_filter("Wish Category", "more ops/dev integration"), "more ops/dev integration"),
                                                    ("docs",create_field_filter("Wish Category", "better documentation"), "better documentation"),
                                                    ("tech_debt",create_field_filter("Wish Category", "less tech debt"), "less tech debt"),
                                                    ("other_culture",create_field_filter("Wish Category", "other cultural"), "other cultural"),
                                                    ("understanding",create_field_filter("Wish Category", "more understanding of DevOps value"), "more understanding of DevOps value"),
                                                    ("time",create_field_filter("Wish Category", "more time"), "more time"),
                                                    ("management_communication",create_field_filter("Wish Category", "management communication"), "management communication"),
                                                    ("other_technical",create_field_filter("Wish Category", "other technical"), "other technical"),
                                                    ("growth_opportunities",create_field_filter("Wish Category", "growth opportunities"), "growth opportunities"),
                                                    ("help_guidance",create_field_filter("Wish Category", "help/guidance"), "help/guidance"),
                                                ],
                                    create_question_answered_filter("Wish Category")
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()

class favorites_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("Favorite parts", [
                                                    ("business",create_field_filter("Favorite Part Category", "Business value added"), "Business value added"),
                                                    ("challenge",create_field_filter("Favorite Part Category", "fun challenge"), "fun challenge"),
                                                    ("cultural",create_field_filter("Favorite Part Category", "other cultural"), "other cultural"),
                                                    ("variety",create_field_filter("Favorite Part Category", "Variety"), "Variety"),
                                                    ("mastery",create_field_filter("Favorite Part Category", "System mastery"), "System mastery"),
                                                    ("tech",create_field_filter("Favorite Part Category", "Cool tech"), "Cool tech"),
                                                    ("automation",create_field_filter("Favorite Part Category", "Joy of automation"), "Joy of automation"),
                                                    ("well_done",create_field_filter("Favorite Part Category", "Satisfaction of a job well done"), "Satisfaction of a job well done"),
                                                    ("technical",create_field_filter("Favorite Part Category", "other technical"), "other technical"),
                                                ],
                                    create_question_answered_filter("Favorite Part Category")
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()

class architecture_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("Architecture", [
                                                    ("micro",create_field_filter("Architecture", "Microservices"), "Microservices"),
                                                    ("mono",create_field_filter("Architecture", "Monolith"), "Monolith"),
                                                    ("neither",create_field_filter("Architecture", "neither"), "neither"),
                                                ],
                                    create_question_answered_filter("Architecture")
                                    )
        
        many_teams_filter = or_(create_field_filter("Teams dedicated to DevOps/SRE", "10+"),
                                create_field_filter("Teams dedicated to DevOps/SRE", "6-9"),
                                create_field_filter("Teams dedicated to DevOps/SRE", "2-5"))
        self.many_teams_analysis = category_analysis("Architecture (>=2 teams)", [
                                                    ("micro",and_(create_field_filter("Architecture", "Microservices"), many_teams_filter), "Microservices"),
                                                    ("mono",and_(create_field_filter("Architecture", "Monolith"), many_teams_filter), "Monolith"),
                                                    ("neither",and_(create_field_filter("Architecture", "neither"), many_teams_filter), "neither"),
                                                ],
                                    and_(create_question_answered_filter("Architecture"), many_teams_filter)
                                    )
        
        few_teams_filter = or_(create_field_filter("Teams dedicated to DevOps/SRE", "0"),
                               create_field_filter("Teams dedicated to DevOps/SRE", "1"))
        self.few_teams_analysis = category_analysis("Architecture (<=1 teams)", [
                                                    ("micro",and_(create_field_filter("Architecture", "Microservices"), few_teams_filter), "Microservices"),
                                                    ("mono",and_(create_field_filter("Architecture", "Monolith"), few_teams_filter), "Monolith"),
                                                    ("neither",and_(create_field_filter("Architecture", "neither"), few_teams_filter), "neither"),
                                                ],
                                    and_(create_question_answered_filter("Architecture"), few_teams_filter)
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
        self.many_teams_analysis.process_row(row)
        self.few_teams_analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()
        self.many_teams_analysis.print_results()
        self.few_teams_analysis.print_results()

class user_access_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("User access", [
                                                    ("on_prem",create_field_filter("User access", "on premise"), "on premise"),
                                                    ("xaas",create_field_filter("User access", "as a service"), "as a service"),
                                                    ("other",create_field_filter("User access", "other"), "other"),
                                                ],
                                    create_question_answered_filter("User access")
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()

class infra_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("Prod Infra", [
                                                    ("cloud",field_contains_filter("Prod Infra", "Cloud"), "Cloud"),
                                                    ("containers",field_contains_filter("Prod Infra", "Containers"), "Containers"),
                                                    ("on_prem",field_contains_filter("Prod Infra", "Dedicated on premise hardware"), "Dedicated on premise hardware"),
                                                    ("dont_know",field_contains_filter("Prod Infra", "dont know"), "dont know")
                                                ],
                                    create_question_answered_filter("Prod Infra")
                                    )
        
        self.hybrid_analysis = category_analysis("Hybrid Prod Infra", [
                                                    ("cloud",and_(field_contains_filter("Prod Infra", "Cloud"), field_contains_filter("Prod Infra", "Dedicated on premise hardware")), "Cloud"),
                                                ],
                                    create_question_answered_filter("Prod Infra")
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
        self.hybrid_analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()
        self.hybrid_analysis.print_results()

class server_count_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("Machine Count", [
                                                    ("one",create_field_filter("Machine Count", "1"), "1"),
                                                    ("tiny",create_field_filter("Machine Count", "2-9"), "2-9"),
                                                    ("small",create_field_filter("Machine Count", "10-99"), "10-99"),
                                                    ("medium",create_field_filter("Machine Count", "100-999"), "100-999"),
                                                    ("large",create_field_filter("Machine Count", "1000-9999"), "1000-9999"),
                                                    ("huge",create_field_filter("Machine Count", "10000+"), "10000+"),
                                                ],
                                    create_question_answered_filter("Machine Count")
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()

class metric_count_analysis(object):
    def __init__(self):
        self.analysis = category_analysis("Metric Count", [
                                                    ("none",create_field_filter("Metric Count", "0"), "0"),
                                                    ("one",create_field_filter("Metric Count", "1-9"), "1-9"),
                                                    ("small",create_field_filter("Metric Count", "10-99"), "10-99"),
                                                    ("medium",create_field_filter("Metric Count", "100-999"), "100-999"),
                                                    ("large",create_field_filter("Metric Count", "1000-9999"), "1000-9999"),
                                                    ("huge",create_field_filter("Metric Count", "10000+"), "10000+"),
                                                ],
                                    create_question_answered_filter("Metric Count")
                                    )
    
    def process_row(self, row):
        self.analysis.process_row(row)
    
    def print_results(self):
        self.analysis.print_results()

def and_(*filters):
    def new_filter(row):
        for row_filter in filters:
            if not row_filter(row):
                return False
        return True
            
    return new_filter

def or_(*filters):
    def new_filter(row):
        for row_filter in filters:
            if row_filter(row):
                return True
        return False
            
    return new_filter

def not_(f):
    def new_filter(row):
        return not f(row)
    
    return new_filter

def parse_args():
    parser = argparse.ArgumentParser(description='Process datafile containing results from DevOps/SRE survey')
    parser.add_argument('file_path',
                    help='The path to the csv file to process')
    parsed_args = parser.parse_args()
    
    return parsed_args


def get_filenames(directory):
    names = []
    for root, _, files in os.walk(directory):
        for f in files:
            names.append(os.path.join(root, f))
    
    return names

def create_field_filter(field, value):
    def value_filter(row):
        return row[field].lower()==value.lower()
            
    return value_filter

def field_contains_filter(field, value):
    def value_filter(row):
        return value.lower() in row[field].lower()
            
    return value_filter

def create_question_answered_filter(field):
    def question_answered(row):
        return row[field]!=''
    
    return question_answered

class rank_analysis(object):
    def __init__(self, analysis_name, column_title, inclusion_filter=lambda r: True):
        self.column_title = column_title
        self.analysis_name = analysis_name
        self.inclusion_filter = inclusion_filter
        
        self.sum = 0
        self.sum_of_squares = 0
        self.count = 0   
    
    def process_row(self, row):
        if not self.inclusion_filter(row):
            return
        
        value = row[self.column_title]
        if value=="" or value=="-1":
            return
        
        value = int(value)
        self.sum += value
        self.sum_of_squares += value**2
        self.count += 1
    
    def print_results(self):
        mean = self.sum*1.0/self.count
        squared_mean = mean**2
        mean_of_squares = self.sum_of_squares*1.0/self.count
        std_dev = math.sqrt(mean_of_squares-squared_mean)
        
        print "{}: {:.1f}+/-{:.1f} (from {} results)".format(self.analysis_name, mean, std_dev, self.count)

class category_analysis(object):
    def __init__(self, analysis_name, category_tuples, total_filter):
        self.category_tuples = category_tuples
        self.analysis_name = analysis_name
        for category_name, _, _ in category_tuples:
            setattr(self, "n_"+category_name, 0)
        
        self.total = 0
        self.total_filter = total_filter
            
    
    def process_row(self, row):
        for category_name, category_filter, _ in self.category_tuples:
            n_in_category = getattr(self, "n_"+category_name)
            n_in_category += 1 if category_filter(row) else 0
            setattr(self, "n_"+category_name, n_in_category)

        self.total += 1 if self.total_filter(row) else 0
    
    def print_results(self):
        dashes = "-"*len(self.analysis_name)
        print "{}\n{}".format(self.analysis_name, dashes)
        for category_name, _, category_description in self.category_tuples:
            print_percentage_results(category_description, getattr(self, "n_"+category_name), self.total)
        print ""

def bernoulli_percent(numerator, denominator):
    p = (numerator * 1.0)/denominator
    std_error = math.sqrt(p*(1-p)/denominator)
    
    return 100*p, 100*std_error

def print_percentage_results(description, numerator, denominator):
    print "{}: {} of {}, or {:.1f}+/-{:.0f}%".format(description, numerator, denominator, *bernoulli_percent(numerator, denominator))

def process_file(data_file):
    reader = csv.DictReader(data_file)
    analyses = analysis_list()
    for row in reader:
        for analysis in analyses:
            analysis.process_row(row)
    
    for analysis in analyses:
        print ""
        analysis.print_results()

if __name__=="__main__":
    main()