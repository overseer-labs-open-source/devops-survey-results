What is this?
-------------

In April 2017, I asked random passers by on reddit /r/programming, /r/devops, and Hacker News
if anyone practicing DevOps or Site Reliability Engineering would take an anonymous survey
covering them, their teams, hopes/fears, codebases, and toolchains. About 200 people responded.

In this directory, you'll find their responses in a CSV file, a pdf showing the survey (in the
editor view, because that was easiest :-) ), and a python script you can use as a jumping off point
to explore the data.
